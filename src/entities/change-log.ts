import { MetadataHelper } from "@neb-sports/mysamay-common-utils";

export class ChangeLog {
    _id: string = null;
    userId: string = null;
    eventType: string = null;
    data: any = null;
    eventTime: Date = null;

    constructor(data: any) {
        Object.keys(this).forEach(key => {
            if(key in data) {
                if(key === "eventTime") {
                    this[key] = new Date(data[key]);
                }
                else {
                    this[key] = data[key];
                }
            }
            else {
                delete this[key];
            }
        });
        if(!this._id) {
            this._id = MetadataHelper.generateUUID();
        }
        if(!this.eventTime)  {
            this.eventTime = new Date();
        }
    }
}