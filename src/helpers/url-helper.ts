import { ContextVariable } from "@neb-sports/mysamay-common-utils";

export class URLHelper {
    baseurl: string;
    contextPath: string = "common-srv";
    createEvent: string = this.contextPath + "/changelogs";

    constructor(contextVar: ContextVariable) {
        this.baseurl = contextVar.baseUrl;
    }

}