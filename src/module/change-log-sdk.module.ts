import { ChangeLogSDK } from '../providers/change-log-sdk';
import { Module } from '@nestjs/common';

@Module({
    imports: [],
    providers: [
      ChangeLogSDK
    ],
    exports: [
      ChangeLogSDK
    ]
  })
export class ChangeLogSDKModule {}
