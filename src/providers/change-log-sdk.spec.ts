import { ChangeLogSDK } from './change-log-sdk';
import { Test, TestingModule } from '@nestjs/testing';



describe('UsersSDK', () => {
    let sdk: ChangeLogSDK;


    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [],
            providers: [ChangeLogSDK],
        }).compile();

        sdk = module.get<ChangeLogSDK>(ChangeLogSDK);
    });

    it('should be defined', () => {
        expect(sdk).toBeDefined();
    });

   

});
