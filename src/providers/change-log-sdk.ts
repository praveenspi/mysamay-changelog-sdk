import { URLHelper } from '../helpers/url-helper';
import { Injectable } from "@nestjs/common";

import urljoin from "url-join";
import { HttpHelper, MetadataHelper, ResponseEntity } from "@neb-sports/mysamay-common-utils";
import { ChangeLog } from '../entities/change-log';

@Injectable()
export class ChangeLogSDK {

    urlHelper: URLHelper;

    constructor() {

    }

    init() {
        this.urlHelper = new URLHelper(MetadataHelper.getContextVariable());
    }

    async createEvents(data: ChangeLog): Promise<ResponseEntity<ChangeLog>> {
        try {
            this.init();
            let url = urljoin(this.urlHelper.baseurl, this.urlHelper.createEvent);
            let headers = this.prepareHeader();
            let resp = await HttpHelper.postRequest<ResponseEntity<ChangeLog>>(url, data, headers);
            return resp.body;
        } catch (error) {
            Promise.reject(error);
        }
    }


    prepareHeader() {
        let contextVar = MetadataHelper.getContextVariable();
        return {
            "X-Access-Token": contextVar.tokenHash,
            "X-Ref-Number": contextVar.refNumber
        }
    }

}
